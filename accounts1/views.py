from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.

def myhome(request):

    my_info_dict = {
        "name": "laks",
        "age": 18,
        "city": "Chennai"
    }

    return render(request, "myhome.html")

def myorder(request):
    return HttpResponse("Showing my order details")

