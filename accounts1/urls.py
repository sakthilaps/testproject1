rom django.urls import path
from . import views

urlpatterns = [
    path("", views.myhome, name="myhome"),
    path("myorder", views.myorder, name="myorder")
]