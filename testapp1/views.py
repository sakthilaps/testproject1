from django.shortcuts import render
from django.http import HttpResponse


# Create your views here.
def home(request):
    emp_id = 101
    emp_name = "anura"
    emp_city = "chennai"

    emp_dict = {"emp_id": emp_id, "emp_name": emp_name, "emp_city": emp_city}

    return render(request, "home.html", context={"emp_info": emp_dict})

def training(request):
    return render(request, 'training.html')    

